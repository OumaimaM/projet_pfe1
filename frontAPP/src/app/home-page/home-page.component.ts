import { Component, OnInit } from '@angular/core';
import { Project } from '../model/project';
import { ServiceJiraService } from '../service-jira.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(private jiraService: ServiceJiraService) { }
    listeProjet!:Project[];
    username!:any;
  ngOnInit(): void {
    this.username=localStorage.getItem('username');

      this.jiraService.getAllProjects().subscribe(
      res =>{

        this.listeProjet=res;
        console.log(this.listeProjet);

      },
      err =>console.log(err)


    )

  }

}
