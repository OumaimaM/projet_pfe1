import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Router } from '@angular/router';

import { ServiceJiraService } from '../service-jira.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),

  });

  constructor(private jiraService:ServiceJiraService, private router:Router) { }

  ngOnInit(): void {
  }

  username:any;
  password:any;


  login(){
console.log(this.userForm.value);
this.jiraService.attemptAuth(this.userForm.value).subscribe(
  res=>{
    console.log(res);
   if(res.msg=='success'){
     console.log(res);
     localStorage.setItem("JSessionID",res.sessionId);
     localStorage.setItem("username",res.username);
    this.router.navigate(['/home']);
   }else{
     this.router.navigate(['/error']);
   }

  },
  err=>{
    this.router.navigate(['/error']);
  },
  ()=>{

  //  this.cookieService.deleteAll();



  }
)


  }

}
