import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn!: boolean;
  constructor(private router:Router) { }

  ngOnInit(): void {
    this.isLoggedIn = localStorage.getItem('username') !=undefined;
    console.log(this.isLoggedIn);
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['']);

  }

}
