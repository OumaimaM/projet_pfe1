import { TestBed } from '@angular/core/testing';

import { ServiceJiraService } from './service-jira.service';

describe('ServiceJiraService', () => {
  let service: ServiceJiraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceJiraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
