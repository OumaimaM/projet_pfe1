import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from './model/project';


@Injectable({
  providedIn: 'root'
})
export class ServiceJiraService {

   urlSignin='http://localhost:8082/auth/signin';

   urlProject='http://localhost:8082/rest/getAllProjects';
   url3="";

  constructor(private http: HttpClient) { }

    // retour sessionid
    attemptAuth(credentials: any): Observable<any> {
   const headers = new HttpHeaders().append('responseType','text');
    return this.http.post<any>(this.urlSignin, credentials,{headers});


 //   const req = new HttpRequest('POST', this.urlSignin, credentials, {
//responseType: 'text'
//});
//
//return this.http.request(req);
    }


    getAllProjects(): Observable<Project[]>{
      let sessionID = localStorage.getItem('JSessionID');
      return this.http.post<any[]>(this.urlProject,sessionID);

    }
}
