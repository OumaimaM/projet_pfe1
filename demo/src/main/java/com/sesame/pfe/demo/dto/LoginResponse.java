package com.sesame.pfe.demo.dto;

import lombok.Data;

@Data
public class LoginResponse {
    private String msg;

    private String sessionId;

    private String username;

}
