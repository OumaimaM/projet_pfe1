package com.sesame.pfe.demo.controller;
import com.sesame.pfe.demo.dto.LoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@CrossOrigin("*")
@RestController
@RequestMapping("/auth")
public class AuthJiraController {

    private final Logger log = LoggerFactory.getLogger(AuthJiraController.class);
    public String baseURL="http://localhost:8080/rest/";
    public String loginURL="auth/1/session";

    public AuthJiraController()
    {

    }

    @PostMapping("/signin")
    public ResponseEntity<?> loginToJira(@RequestBody UserForm userLogin, HttpServletResponse response)
    {
        LoginResponse responseSignin=new LoginResponse();
        String loginResponse="";
        URL url=null;
        HttpURLConnection conn=null;
        HttpHeaders httpHeader= new HttpHeaders();
        String input="";
        OutputStream os=null;
        BufferedReader br=null;
        String output=null;
        String jSessionID="";
        try
        {
            //create url object
            url =new URL(baseURL+loginURL);
            //use the url to create connection
            conn=(HttpURLConnection) url.openConnection();
            //set properties
            conn.setDoOutput(true);
            //conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/json");
            //create json post data
            input="{\"username\":\"" + userLogin.getUsername() + "\",\"password\":\"" + userLogin.getPassword() + "\"}";
            //send our request
            os=conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            //handle our response
            if(conn.getResponseCode()==200)
            {
                br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                while((output=br.readLine()) !=null)
                {
                    loginResponse+=output;
                    JSONParser parser = new JSONParser();
                    Object obj=parser.parse(loginResponse);
                    JSONObject jsonObject = (JSONObject) obj;
                    JSONObject sessionJsonObj = (JSONObject) jsonObject.get("session");
                    jSessionID = (String) sessionJsonObj.get("value");
                   // Cookie cookie = new Cookie("SESSIONID",jSessionID);
                /*  HttpCookie httpCookie =  ResponseCookie.from("SESSIONID", jSessionID)
                     .httpOnly(true)
                     .path("/")
                     .build();*/

              //httpHeader.add(HttpHeaders.SET_COOKIE,httpCookie.getValue());
                        responseSignin.setMsg("success");
                        responseSignin.setSessionId(jSessionID);
                        responseSignin.setUsername(userLogin.getUsername());
                    return ResponseEntity.ok().headers(httpHeader).body(responseSignin);



                }
                conn.disconnect();
            }
        }
        catch(Exception ex)
        {
            //handle errors
            loginResponse="ERROR";
        }
        System.out.println("LoginResponse");
        System.out.println(loginResponse);


        return ResponseEntity.ok().headers(httpHeader).body("error");
    }

















































}
