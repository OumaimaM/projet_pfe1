package com.sesame.pfe.demo.controller;
import lombok.Data;

@Data
public class UserForm {

    private String username;

    private String password;
}
