package com.sesame.pfe.demo.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Project {

    private String expand;
    private String self;
    private String id;
    private String key;
    private String name;
//    private Object avatarUrls;
    private String projectTypeKey;

    @JsonCreator
    public Project(@JsonProperty("expand") String expand, @JsonProperty("self") String self, @JsonProperty("id")String id, @JsonProperty("key") String key, @JsonProperty("name")String name, @JsonProperty("projectTypeKey")String projectTypeKey) {
        super();
        this.expand = expand;
        this.self = self;
        this.id = id;
        this.key = key;
        this.name = name;
        this.projectTypeKey = projectTypeKey;
    }
}
