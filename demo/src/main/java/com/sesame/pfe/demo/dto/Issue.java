package com.sesame.pfe.demo.dto;


import java.util.List;

public class Issue {

    private String expand;
    private Integer startAt;
    private Integer maxResults;
    private Integer total;
    private List<DetailIssue> issues;

}
