package com.sesame.pfe.demo.controller;


import com.atlassian.jira.rest.client.api.JiraRestClient;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;


@CrossOrigin("*")
@RestController
@RequestMapping("/rest")
public class JiraController {
    private final Logger log = LoggerFactory.getLogger(JiraController.class);

    public String loginURL = "auth/1/session";
    public String baseURL = "http://localhost:8080/rest/";


    @Autowired
  private RestTemplate restTemplate;

    public JiraRestClient jiraRestClient;


    public JiraController() {

    }
    @GetMapping("/getAllIssues/{idProjet}")
    public List<?> getAllIssuesForProject(@RequestParam Integer idProjet){
       // RestTemplate restTemplate = new RestTemplate();

     try{
        // String url=baseURL +"api/2/search?jql=project="+idProjet +"&maxResults=100";
         String url ="http://localhost:8080/rest/api/2/search?jql=project=10201&maxResults=100";
     ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity(url,Object[].class);
         Object[] objects = responseEntity.getBody();
         MediaType contentType = responseEntity.getHeaders().getContentType();
         HttpStatus statusCode = responseEntity.getStatusCode();
     }catch(Exception ex)
     {
         System.out.println("error in getJsondata: "+ ex.getMessage());

     }



        return null;
    }

    @PostMapping("/getAllProjects")
    public List<?> getAllProjects(@RequestBody String sessionID){
        String jsonData="";
        int size;
        String name="";
        List<?> list = new ArrayList<>();

        try{
            //get all issues  for projet ID
          //  URL url=new URL(baseURL +"api/2/search/" +"?jql=project=10000" +"&maxResults=100");
     //  http://localhost:8080/rest/api/2/project

            //getallprojects
            URL url=new URL(baseURL +"api/2/project");

            //get the board listes
            //URL url=new URL(baseURL +"agile/1.0/board");
            //get all sprint for project ID
            //URL url=new URL(baseURL +"agile/1.0/board/1/sprint");
            //get all issue for sprint ID
            //URL url=new URL(baseURL +"agile/1.0/board/1/sprint/1/issue");
            //get the current user
            // URL url=new URL(baseURL + "auth/1/session");
            String cookie ="JSESSIONID=" + sessionID;
            HttpURLConnection conn=(HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestProperty("Cookie",cookie);
            if(conn.getResponseCode()==200){
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String output="";
                while((output = br.readLine()) !=null){
                    jsonData +=output;
                    JSONParser parser = new JSONParser();
                   // Object obj=parser.parse(jsonData);
                   // JSONObject jsonObject = (JSONObject) obj;

                    Object obj  = parser.parse(jsonData);
                    JSONArray array = new JSONArray();
                    array.add(obj);
                     size= array.size();
                    list = convertObjectToList(obj);
                }
//

                conn.disconnect();

            }
        }catch(Exception ex)
        {
            System.out.println("error in getJsondata: "+ ex.getMessage());
            sessionID="ERROR";
        }
        return list;

    }
    public static List<?> convertObjectToList(Object obj) {
        List<?> list = new ArrayList<>();
        if (obj.getClass().isArray()) {
            list = Arrays.asList((Object[])obj);
        } else if (obj instanceof Collection) {
            list = new ArrayList<>((Collection<?>)obj);
        }
        return list;
    }

}


